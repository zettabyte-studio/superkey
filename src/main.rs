use std::{
    fs,
    io::{prelude::*, BufReader},
    net::{TcpListener, TcpStream},
    thread,
    time::Duration,
    env,
};
use reqwest::Error;
use std::fs::File;
use daemonize::Daemonize;
use zettabyte_proxy::ThreadPool;

fn main() {
    let args: Vec<String> = env::args().collect();
    if args.len() < 1 {
        panic!("must specify working directory (working directory must have a 'frontend' folder containing the superkey frontend)");
    }
    
    let working_dir = &args[1];

    let listen = "127.0.0.1:7878";

    println!("=> superkey starting...\n");
    println!("=> [i] running on version {}", env!("CARGO_PKG_VERSION"));

    let pool = ThreadPool::new(64);
    println!("=> [i] main thread pool initialized");

    let listener = TcpListener::bind("127.0.0.1:7878").unwrap();
    println!("=> [i] superkey proxy listening on {}\n", listen);

    let stdout = File::create("/tmp/superkey.out").unwrap();
    let stderr = File::create("/tmp/superkey.err").unwrap();

    let daemonize = Daemonize::new()
        .pid_file("/tmp/superkey.pid") // Every method except `new` and `start`
        .chown_pid_file(true)      // is optional, see `Daemonize` documentation
        .working_directory(&working_dir) // for default behaviour.
        .user("nobody")
        .group("daemon") // Group name
        .group(2)        // or group id.
        .umask(0o777)    // Set umask, `0o027` by default.
        .stdout(stdout)  // Redirect stdout to `/tmp/daemon.out`.
        .stderr(stderr)  // Redirect stderr to `/tmp/daemon.err`.
        .privileged_action(|| "Executed before drop privileges");
    
    //match daemonize.start() {
    //    Ok(_) => println!("Success, daemonized"),
    //    Err(e) => eprintln!("Error, {}", e),
    //}

    for stream in listener.incoming() {
        let stream = stream.unwrap();

        pool.execute(|| {
            handle_connection(stream);
        })
    }
}

fn handle_connection(mut stream: TcpStream) {
    let buf_reader = BufReader::new(&mut stream);

    let request_binding = buf_reader
        .lines()
        .next()
        .unwrap()
        .unwrap();
    let request_split: Vec<&str> = request_binding
        .split(" ")
        .collect();

    println!("=> [r]\n   got: '{:?}'\n", &request_split);

    match stream.write_all(route_request(&request_split).unwrap().as_bytes()) {
        Ok(x) => x,
        Err(e) => {
            eprintln!("=> [!] internal error when serving request: '{}'", e);
        },
    }

    /*
    let status_line = "HTTP/1.1 200 OK";
    let send_file = fs::read_to_string("index.html").unwrap();
    let send_length = send_file.len();

    let response = 
        format!("{status_line}\r\nContent-Length: {send_length}\r\n\r\n{send_file}");

    stream.write_all(response.as_bytes()).unwrap();

    println!("=> [r] {:#?}", http_request);
    */
}

fn route_request(request_data: &Vec<&str>) -> Result<String, Error> {
    match request_data[1] {
        "/" /* /frontend/index/html */ => {
            let status_line = format!("{} 200 OK", request_data[2]);
            let send_file = fs::read_to_string("frontend/index.html").unwrap();
            let send_length = send_file.len();
            let send_type = "text/html";

            Ok(format!(
                "{status_line}\r\nContent-Length: {send_length}\r\nContent-Type: {send_type}\r\n\r\n{send_file}"
            ))
        },

        "/frontend/index/css" => {
            let status_line = format!("{} 200 OK", request_data[2]);
            let send_file = fs::read_to_string("frontend/index.css").unwrap();
            let send_length = send_file.len();
            let send_type = "text/css";

            Ok(format!(
                "{status_line}\r\nContent-Length: {send_length}\r\nContent-Type: {send_type}\r\n\r\n{send_file}"
            ))
        },

        "/frontend/index/js" => {
            let status_line = format!("{} 200 OK", request_data[2]);
            let send_file = fs::read_to_string("frontend/index.js").unwrap();
            let send_length = send_file.len();
            let send_type = "text/javascript";

            Ok(format!(
                "{status_line}\r\nContent-Length: {send_length}\r\nContent-Type: {send_type}\r\n\r\n{send_file}"
            ))
        },

        "/frontend/proxy-placehold/html" => {
            let status_line = format!("{} 200 OK", request_data[2]);
            let send_file = fs::read_to_string("frontend/proxy-placehold.html").unwrap();
            let send_length = send_file.len();
            let send_type = "text/html";

            Ok(format!(
                "{status_line}\r\nContent-Length: {send_length}\r\nContent-Type: {send_type}\r\n\r\n{send_file}"
            ))
        },

        "/frontend/proxy-placehold/css" => {
            let status_line = format!("{} 200 OK", request_data[2]);
            let send_file = fs::read_to_string("frontend/proxy-placehold.css").unwrap();
            let send_length = send_file.len();
            let send_type = "text/css";

            Ok(format!(
                "{status_line}\r\nContent-Length: {send_length}\r\nContent-Type: {send_type}\r\n\r\n{send_file}"
            ))
        },

        "/sw" => {
            let status_line = format!("{} 200 OK", request_data[2]);
            let send_file = fs::read_to_string("frontend/sw.js").unwrap();
            let send_length = send_file.len();
            let send_type = "text/javascript; charset=UTF-8";

            Ok(format!(
                "{status_line}\r\nContent-Length: {send_length}\r\nContent-Type: {send_type}\r\nService-Worker-Allowed\r\n\r\n{send_file}"
            ))
        },

        /*"/sleep" => {
            thread::sleep(Duration::from_secs(5));

            let status_line = format!("{} 200 OK", request_data[2]);
            let send_file = fs::read_to_string("index.html").unwrap();
            let send_length = send_file.len();
            let send_type = "text/html";

            Ok(format!(
                "{status_line}\r\nContent-Length: {send_length}\r\nContent-Type: {send_type}\r\n\r\n{send_file}"

            ))
        },*/

        _ if request_data[1].starts_with("/sw/sk") => {
            println!("=> [r i] proxy request");
            
            //let split_raw = request_data[1].split("-");
            let split = &request_data[1][7..];
            //let split = request_data[1].split("//").collect::<Vec<&str>>(); // i hate ownership

            //if request_data[0].split("-").count() as i8 == 3 { // i hate ownership x2
                dbg!(&split);
                let status_line = format!("{} 200 OK", request_data[2]);

                let mut res;

                if split.starts_with("https://") || split.starts_with("http://") {
                    res = reqwest::blocking::get(split)?;
                } else {
                    res = reqwest::blocking::get(format!(
                        "https://duckduckgo.com/?q={}",
                        &split.replace(" ", "+")))?;
                }

                let mut body = res.text()?;
                //dbg!(&body);

                let send_length = body.len();
                // we can't use a mime type in here, because
                // for a proxy request the content type is
                // unknown.
                // using text/* as a mime type caused
                // firefox to download the webpage instead
                // of downloading it.
                // this wasn't tested on any other browsers
                // however, so this odd behaviour may just
                // be a firefox thing.

                return Ok(format!(
                    "{status_line}\r\nContent-Length: {send_length}\r\n\r\n{body}"
                ))
            /*} else {
                let status_line = format!("{} 200 OK", request_data[2]);
                let send = "baller";
                let send_length = send.len();

                return Ok(format!(
                    "{status_line}\r\nContent-Length: {send_length}\r\n\r\n{send}"
                ))
            }*/
        },

        &_ => {
            let status_line = format!("{} 404 Not Found", request_data[2]);
            let send = "superkey: 404: not found";
            let send_length = send.len();
            let send_type = "text/plain";

            Ok(format!(
                "{status_line}\r\nContent-Length: {send_length}\r\nContent-Type: {send_type}\r\n\r\n{send}"

            ))
        },
    }
}
