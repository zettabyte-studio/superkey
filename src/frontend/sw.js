var current_page;
var root_url;

function post_message(message, client_id) {
  const client = await clients.get(event.clientId);
  if (client) {
    client.postMessage({msg: message});
  }
}

self.addEventListener("message", (event) => {
  if (event.data.type === "root_url") {
    root_url = event.data.msg;
  } else if (event.data.type === "current_page") {
    current_page = event.data.msg;
  }
});

self.addEventListener("fetch", (event) => {
  if (!current_page || !root_url) {

  }
  if (event.clientId) {
      post_message(`[superkey sw] [i] => intercepting request: ${event.request.url}`, event.clientId);
    }
  }
  let target_url = event.request.url.replace(root_url, current_page);

  event.respondWith(
    fetch(`/sw/sk/${target_url}`)
  );
});
