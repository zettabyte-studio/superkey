const url_go = document.getElementById("url-go");
const url_input = document.getElementById("url-input");
const page_iframe = document.getElementById("page");

navigator.serviceWorker.addEventListener("message", (event) => {
  console.log(event.data.msg);
});

const register_autoproxy_sw = async () => {
  if ("serviceWorker" in navigator) {
    try {
      const registration = await navigator.serviceWorker.register("/sw", {
        scope: "/sw/",
      });
      if (registration.installing) {
        console.log("[superkey] [i] => autoproxy service worker installing...");
      } else if (registration.waiting) {
        console.log("[superkey] [i] => autoproxy service worker waiting to activate...");
      } else if (registration.active) {
        console.log("[superkey] [i] => autoproxy service worker active, proxy online");
      }
    } catch (ERR) {
      console.error(`[superkey] [!] => autoproxy service worker registration failed (${ERR})`);
    }
  }
};

function proxy_website(url) {
  console.log(`[superkey] [i] => intercepting direct request and proxying '${url}'`);
  var http_req = new XMLHttpRequest();
  http_req.open("GET", `/sw/sk/${url}`, false);
  http_req.send(null);
  return http_req.responseText;
}

function url_go_click_hook() {
  console.log(`[superkey] [i] => new proxy request to '${url_input.value}'`);
  page_iframe.setAttribute("src", `/sw/sk/${url_input.value}`);
  navigator.serviceWorker.controller.postMessage({
    type: "current_page",
    msg: url_input.value;
  });
}

register_autoproxy_sw();

/*
if ('serviceWorker' in navigator) {
  window.addEventListener('load', () => {
    navigator.serviceWorker.register('/frontend/sw/js').then((REGST) => {
      console.log('[superkey] [i] => service worker registered with scope: ', REGST.scope);
    }, (ERR) => {
      console.log('[superkey] [!] => service worker registration failed: ', ERR);
    });
  });
}
*/
